# c:\users\vis\appdata\local\programs\python\python39\python.exe -m pip install --upgrade pip
from datetime import date
import os
import platform
import time
import sys
import requests
import json
import cognitive_face as CF
from PIL import Image, ImageDraw, ImageFont
REGISTER = []
subscription_key = None
SUBSCRIPTION_KEY = '388442f09fd2422fa3feb6004a8254c6'
BASE_URL = 'https://luxgie6996.cognitiveservices.azure.com/face/v1.0/'
CF.BaseUrl.set(BASE_URL)
CF.Key.set(SUBSCRIPTION_KEY)

def emotions(picture):
    """Function returns a dictionary with all  information 
    about the person.

    Args:
        picture (list): Dictionary and list with the information about the image.

    Returns:
        str: Dictionary with faces.
    """

    #headers = {'Ocp-Apim-Subscription-Key': 'e70e11c9cb684f21b8b37313fd60e5bc'}
    image_path = picture
    # https://docs.microsoft.com/en-us/azure/cognitive-services/computer-vision/quickstarts/python-disk
    # Read the image into a byte array
    image_data = open(image_path, "rb").read()
    headers = {'Ocp-Apim-Subscription-Key': SUBSCRIPTION_KEY,
               'Content-Type': 'application/octet-stream'}
    params = {
        'returnFaceId': 'true',
        'returnFaceLandmarks': 'false',
        'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise',
    }
    response = requests.post(
        BASE_URL + "detect/", headers=headers, params=params, data=image_data)
    analysis = response.json()
    dic = analysis[0]
    fa = dic['faceAttributes']
    return analysis



def print_register():
    """Query history

    Returns:
            {list} -- Save all user queries
    """
    for i in REGISTER:
        print(i)




def clear_the_terminal():
    """This function clear the terminal.
    """    
    time.sleep(5)
    if platform.system() == "Windows":
        os.system("cls")
    else:
        os.system("clear")  

def clear_the_terminal2():
    """This function clear the terminal.
    """    
    time.sleep(3)
    if platform.system() == "Windows":
        os.system("cls")
    else:
        os.system("clear")






# a.
 
def a(data):
    """The function returns the information about the ages, and associated with each face.

    Args:
        dates (list): Returns a content of the image.
    """
    print("\n" "The amount faces are: ", len(data), "And your ages are: \n")
    print()
    age_as = list()
    for a_1 in data:
        faceattibutes_a = a_1["faceAttributes"]
        age_a = faceattibutes_a["age"]
        age_as.append(age_a)
    print(age_a)
    register = "Show the amount of faces in the image: " + str("\n", len(data), "\n")
    REGISTER.append(register)

# b.

def b(data):
    """The function show a list with all the ages of the people

    Args:
        dates (list): Content the information of the image.
    """
    little_list_b = []
    for b_1 in data:
        faceattibutes_b = b_1["faceAttributes"]
        age_b = faceattibutes_b["age"]
        little_list_b.append(age_b)
        little_list_b.sort(reverse=True)
    
    print(little_list_b)
    print("\n" "The older age is: ", little_list_b[0])
    register = "The older age is", "\n", little_list_b[0], little_list_b, "\n"
    REGISTER.append(register)

# C. 

def c(data):

    """The function returns a list with sublists to show the faceId, age and gender of the faces present in the image. 

    Args:
        dates (list): content a dictionary of the image.

    Returns:
        str: returns the majors temporary and the new list.
    """

    general_list_c1 = []
    for c_1 in data:
        faceId_c1 = c_1["faceId"]
        faceAttributes_c1 = c_1["faceAttributes"]
        age_c1 = faceAttributes_c1["age"]

        gender_c1 = faceAttributes_c1["gender"]
        little_list_c1 = [faceId_c1, age_c1, gender_c1]
        general_list_c1.append(little_list_c1)

    def high_list(list_1):
        high_temp = list_1[0]
        for x in list_1:
            if x[1] < high_temp[1]:
                high_temp = x
        return high_temp

    def ord_insertion(list_1):
        new_list = []
        while len(list_1) > 0:
            high = high_list(list_1)
            new_list.append(high)
            list_1.remove(high)
        return (new_list)
    c7=(ord_insertion(general_list_c1), "\n")
    print(c7)
    register = "list with sublist and faceId: ", "\n", c7, "\n"
    REGISTER.append(register)

# d.

def d(data):
    """The function returns a list with sublists to show the faceId and the age of the faces 
       present in the image, it also arranges them in ranges of 10 according to age.

    Args:
        dates (list): returns the consulted content of an image, such as the faceId and the age.
    """
    general_list2 = []
    for d_1 in data:
        faceAttributes_d1 = d_1["faceAttributes"]
        age_d1 = faceAttributes_d1["age"]
        gender_d1 = faceAttributes_d1["gender"]
        little_list2 = [age_d1, gender_d1 ]
        general_list2.append(little_list2)
    # print(general_list2)

    def bubble_sort_d(oneList):
        """The function sorts the list.

        Args:
            oneList (list): Contains a series of data such as faceId, age and gender
        """        
        exchanges = True
        numPast = len(oneList)-1
        while numPast > 0 and exchanges:
            exchanges = False
        for i in range(numPast):
            if oneList[i]>oneList[i+1]:
                exchanges = True
                temp = oneList[i]
                oneList[i] = oneList[i+1]
                oneList[i+1] = temp
        numPast = numPast-1

    oneList= general_list2
    bubble_sort_d(oneList)
    # print(oneList)

    def create_ranges(onelist):
        """Sort the list in ranges of 10 to 10.

        Args:
            onelist (list): The list comes normal, and ends up ordered in ranges of 10.
        """        
        listaNueva = []
        cont = 10
        limite = (onelist[len(onelist)-1][0]//10+1)*10

        while cont <= limite:
            nuevo = []
            for x in onelist:
                if (x[0] < cont):
                    nuevo.append(x)
            i = 0
            while i < len(onelist)-1:
                if (onelist[i] in nuevo):
                    onelist.remove(onelist[i])
                    i -= 1
                i += 1
            listaNueva.append(nuevo)
            cont += 10
        return listaNueva
    d7 = create_ranges(oneList)
    print(d7)
    register = "Show the age and gender in range of 10: " + str("\n", d7, "\n")
    REGISTER.append(register)

# e.

def e(data):
    """Function that returns the ages associated with their average and their range

    Args:
        dates {list} -- The list with all the ages and FacesId

    Returns:
        {list} -- Returns the range of ages.
    """
    new_list_e1 = []
    pro = 0
    for e_1 in data:
        faceId_e1 = e_1["faceId"]
        faceAttributes_e1 = e_1["faceAttributes"]
        ages_e1 = faceAttributes_e1["age"]
        pro += ages_e1
        new_list_e2 = [faceId_e1, ages_e1]
        new_list_e1.append(new_list_e2)

    #print(new_list_e3, "\n")

        # print(new_list_e1)
        # print(pro)
    def range_age(liste_e):
        new_list_e3 = []
        prom = pro // len(data)
        range_e1 = prom
        range_e2 = prom - 5
        print("\n", "The average of age is: ", prom, "\n")
        print("\n" "In the case that no age is close to the average, it will return an empty list" "\n")
        ind = 0
        while ind < len(liste_e):
            if len(liste_e) == 0:
                return None

            else:
                for e_2 in liste_e[1:]:
                    if e_2[1] >= range_e2 and e_2[1] <= range_e1:
                        new_list_e3.append(e_2)
                    ind += 1
                return new_list_e3
    e = range_age(new_list_e1)
    print(e)
    register = "Ages with a range +- 5 years: " + str("\n", e, "\n") 
    REGISTER.append(register)
    
    print("The people closest to the average age with a range of + - 5 the years are ", range_age(new_list_e1), "\n")

# f.
 
def f(data):
    general_list_f = []
    for f_1 in data:
        faceAttributes_f = f_1["faceAttributes"]
        age_f = faceAttributes_f["age"]
        gender_f = faceAttributes_f["gender"]
        little_list_f1 = [age_f, gender_f]
        general_list_f.append(little_list_f1)
    #print(general_list_f)




    def Partitioning(listf):
        """Partition the list by taking a pivot

        Args:
            lista {list} -- list with the age and the gender

        Returns:
            {list}  -- The list partioning
        """    
        pivote = listf[0]
        high = list()
        less = list()
        for i in range(1, len(listf)):
            if listf[i] < pivote:
                less.append(listf[i])
            else:
                high.append(listf[i])
        return less, pivote, high

    Partitioning(general_list_f)
    def quicksort(listf):
        """Quick sort: sort the list

        Args:
            listf {list} -- messy list

        Returns:
            {list} -- The list sorted
        """    
        if len(listf) < 2:
            return listf
        less, pivote, high = Partitioning(listf)
        return quicksort(less) + [pivote] + quicksort(high)
    quicksort(general_list_f)
    #print()



    def male_female(listf):
        """Separate the list by gender, male, female

        Args:
            listf {list} -- The list with genders male, famale and ages

        Returns:
            {list} -- The list separated by gender, male and female
        """
        male = []
        female = []
        for x in listf[:]:
            if x[1] == "male":
                male.append(x)
                listf.remove(x)
                male.sort()
            elif x[1] == "female":
                female.append(x)
                listf.remove(x)  
                female.sort()  

        return male, female
    
    f = male_female(general_list_f)
    print("\n", "The result is: ", f, "\n")
    register = "male and female separated: " + str("\n", f, "\n")
    REGISTER.append(register)

# g. 

def g(data):
    list_g2 = []
    for g_1 in data:    
        faceAttributes = g_1["faceAttributes"]
        faceId = g_1["faceId"]
        age = faceAttributes["age"]
        gender = faceAttributes["gender"]
        list_g1 = [faceId, age, gender]
        list_g2.append(list_g1)
    print(list_g2, "\n")


    def higher_list (listg):
        """Extract the largest 

        Args:
            listg {list}: Sort the largest and put it in a new list

        Returns:
            {list}: returns the largest 
        """    
    
        high_temp = listg[0]
        for x in listg [1:]:
            if x[1] < high_temp[1]:
                high_temp = x
        return high_temp

    def sort_insertion (listg):
        """List with the faceIds and Ages

        Args:
            listg {list}: Messy list

        Returns:
            {list}: Retruns a sorted list
        """    
        new_list = []
        while len(listg) > 0:
            high = higher_list(listg)
            new_list.append(high)
            listg.remove(high)
        return (new_list)


    High = (sort_insertion(list_g2), "\n")
    print(High, "\n")
    register = "The list sorted with insertion sorted: " + str("\n", High, "\n")
    REGISTER.append(register)

# h. 

def h(data):
    """The function shows the faceId and the most predominant hair color.

    Args:
         dates (list): The list shows the faceId and the most predominant hair color.

    Returns:
            list: Returns a list with the faceId and the hair color that most predominated among the people.
    """    
    for h in data:
        faceAtributes_h = h["faceAttributes"]
        faceId_h = h["faceId"]
        hair_h = faceAtributes_h["hair"]
        hair_color_h = hair_h["hairColor"]
        general_listh = []
        for h1 in hair_color_h:
            color_h = h1["color"]
            confidence_h = h1["confidence"]
            list_h = [faceId_h, color_h, confidence_h]
            
            general_listh.append(list_h)
                
        def return_major(general_listh):
            """The function returns the largest data, which in this case would be which hair color predominates.

            Args:
                general_listh (list): The function contains requested data.

            Returns:
                    list: A list already with the largest that was requested.
            """            
            if len(general_listh) == 0:
                return(None)
            else:
                major_temporary = general_listh[0]
                for h2 in general_listh[1:]:
                    if h2[1]>major_temporary[1]:
                        major = h2
                return major_temporary
        h = return_major(general_listh)
        print('The most predominant color is: ', h, "\n")
        register = "The most predominant color: " + str("\n", h, "\n")
        REGISTER.append(register)
        
# i.

def i(data):

    for i in data:
        faceAtributes_i = i["faceAttributes"]
        faceId_i = i["faceId"]
        emotion_i = faceAtributes_i["emotion"]  
        general_list_i = []
        for i1 in emotion_i:
            anger_i = emotion_i["anger"]
            contempt_i = emotion_i["contempt"]
            disgust_i = emotion_i["disgust"]
            fear_i = emotion_i["fear"]
            happiness_i = emotion_i["happiness"]
            neutral_i = emotion_i["neutral"]
            sadness_i = emotion_i["sadness"]
            surprise_i = emotion_i["surprise"]
            list_i = [faceId_i, "anger: ", anger_i, "contempt: ", contempt_i, "disgust: ", disgust_i, "fear: ", fear_i, "happiness: ", happiness_i, "neutral: ", neutral_i, "sadness: ", sadness_i, "surprise: ", surprise_i]  

            general_list_i.append(list_i)
                
        def return_major2(general_list_i):
            """ The function returns the most predominant emotion.

            Args:
                general_list_i (list): The list contains the data of the emotions.
            """            
            if len(general_list_i) == 0:
                return(None)
            else:
                new_listi = list()
                for i2 in general_list_i:
                    i3 = 2
                    major_tag = i2[1]
                    major = 0
                    while i3 <= len(i2):
                        if major<i2[i3]:
                            major_tag = i2[i3-1]
                            major = i2[i3]
                        i3+=2
                        
                print('El faceId es: ', i2[0])
                print("The most predominant emotion is: ",  major_tag, major, "\n")
                new_listi.append(major_tag)
                new_listi.append(major)
        return_major2(general_list_i)
    register = "The most predominat emotion: " + str("\n", general_list_i, "\n")
    REGISTER.append(register)

# j. 
       
def j(dates):
    for j in data:
        faceAtributes_j = j["faceAttributes"]
        faceId_j = j["faceId"]
        accessories_j = faceAtributes_j["accessories"]
        if len(accessories_j) == 0:
            print("The person: ", faceId_j, "Does not carry accessories")
        else:
            print("The person: ", faceId_j, "The accessories it carries are: ",  accessories_j)
    register = "Accesories: " + str("\n", accessories_j, "\n")
    REGISTER.append(register)

# k.

def k(data):

    def emotions(picture):
        #headers = {'Ocp-Apim-Subscription-Key': 'e70e11c9cb684f21b8b37313fd60e5bc'}
        image_path = picture
        #https://docs.microsoft.com/en-us/azure/cognitive-services/computer-vision/quickstarts/python-disk
        # Read the image into a byte array
        image_data = open(image_path, "rb").read()
        headers = {'Ocp-Apim-Subscription-Key': SUBSCRIPTION_KEY,
        'Content-Type': 'application/octet-stream'}
        params = {
            'returnFaceId': 'true',
            'returnFaceLandmarks': 'false',
            'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise',
        }
        response = requests.post(
                                BASE_URL + "detect/", headers=headers, params=params, data=image_data)
        analysis = response.json()
        faces = []
        for face in analysis:
            fr = face ['faceRectangle']
            faces.append(fr)
        show_image(picture,faces)
        
    def show_image(path, faces):
        image = Image.open(path)
        draw = ImageDraw.Draw(image)
        for fr in faces:
            top = fr['top']
            left = fr['left']
            width = fr['width']
            height = fr['height']
            draw.rectangle((left, top, left+width, top+height), outline='red', width=5)    
        font = ImageFont.truetype("public_semana-8_Arial_Unicode.ttf", 50)
        draw.text((50,50), "Person(s)", font= font, fill="red")
        image.show() 

    if __name__ == "__main__":
        picture = input("Ingrese el path de la imagen:")
        emotions(picture)
    NEW = "\n" "FaceRectangle Consult" "\n"
    REGISTER.append(NEW)
# l. 

def l(data):
    emotion_list2 = list()

    pro = 0
    for m_1 in data:
        faceAtributes_m = m_1["faceAttributes"]
        faceId = m_1["faceId"]
        age_m = faceAtributes_m["age"]
        pro += age_m 
        emotion = faceAtributes_m["emotion"]
        happiness = emotion["happiness"]
        new_list = list()
        new_list.append(happiness)
        new_list.append(faceId)
        emotion_list2.append(new_list)
    #print(emotion_list2)
            
            
    prom = pro // len(data)

    def happy(listm):
        """This function brings out the person(s) with the highest percentage of happiness

        Args:
            listm {list} -- List with all people's emotions

        Returns:
                {list} -- List with the person(s) with the highest percentage of happiness
        """    
        z = listm[1]
        listm1 = []
        for m_2 in listm:
            if m_2 > z:
                m_2 = z
            elif m_2 == z:
                listm1.append(z)
        listm1.append(m_2)


        return listm1
            
    happy(emotion_list2)
    my_list = happy(emotion_list2)

    for m_4 in my_list:
        if m_4[0] < prom:
            print("\n" "The average is: ", prom, ", The person(s) with the greatest happiness and that is below the average is: ", my_list, "\n")
            break
        else:
            print("\n" "There is no person with below average happiness.", prom, "The happiest person is: ", my_list, "\n")
            break
    register = "The average age (happiness): " + str("\n", my_list, "\n")
    REGISTER.append(register)

# m. 

def m(data):
    emotion_list2 = list()

    pro = 0
    for m_1 in data:
        faceAtributes_m = m_1["faceAttributes"]
        faceId = m_1["faceId"]
        age_m = faceAtributes_m["age"]
        pro += age_m 
        emotion = faceAtributes_m["emotion"]
        happiness = emotion["happiness"]
        new_list = list()
        new_list.append(happiness)
        new_list.append(faceId)
        emotion_list2.append(new_list)
    #print(emotion_list2)
        
        
    prom = pro // len(data)

    def happy(listm):
        """This function brings out the person(s) with the highest percentage of happiness

        Args:
            listm {list} -- List with all people's emotions

        Returns:
            {list} -- List with the person(s) with the highest percentage of happiness
        """    
        z = listm[1]
        listm1 = []
        for m_2 in listm:
            if m_2 > z:
                m_2 = z
            elif m_2 == z:
                listm1.append(z)
        listm1.append(m_2)


        return listm1
        
    happy(emotion_list2)
    my_list = happy(emotion_list2)

    for m_4 in my_list:
        if m_4[0] > prom:
            print("\n" "The average is: ", prom, "The person(s) with the greatest happiness and who is above the average age is", my_list, "\n")
        else:
            print("\n" "There is no person(s) with a happiness range close to the average age. Average = ", prom, "The happiest person is: ", my_list, "\n")
            break
    register = "The average age (happiness): " + str("\n", my_list, "\n")   
    REGISTER.append(register)

# n. 

def n(data):
    def emotions(picture):
        #headers = {'Ocp-Apim-Subscription-Key': 'e70e11c9cb684f21b8b37313fd60e5bc'}
        image_path = picture
        #https://docs.microsoft.com/en-us/azure/cognitive-services/computer-vision/quickstarts/python-disk
        # Read the image into a byte array
        image_data = open(image_path, "rb").read()
        headers = {'Ocp-Apim-Subscription-Key': SUBSCRIPTION_KEY,
        'Content-Type': 'application/octet-stream'}
        params = {
            'returnFaceId': 'true',
            'returnFaceLandmarks': 'false',
            'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise',
        }
        response = requests.post(
                                BASE_URL + "detect/", headers=headers, params=params, data=image_data)
        analysis = response.json()
        faces = []
        beards = []
        for face in analysis:
            faceAtributes_n = face["faceAttributes"]
            facialHair = faceAtributes_n["facialHair"]
            sideburns = facialHair["sideburns"]
            moustache = facialHair["moustache"]
            beard = facialHair["beard"]
            new = [sideburns+moustache+beard]
            beards.append(new)
            #fr = face ['faceRectangle']
            #faces.append(fr)
            
        for b in beards[0:]:
            v = beards[1]
            if b > v:
                b = v

            else:

                fr = face ['faceRectangle']
                faces.append(fr)

                show_image(picture,faces)
        
            
        
        

    def show_image(path, faces):
        """Return the image with the facerectangle

        Args:
            path str: -- Image location
            faces {list} -- contains the facerectangle
        """        
        image = Image.open(path)
        draw = ImageDraw.Draw(image)
        for fr in faces:
            top = fr['top']
            left = fr['left']
            width = fr['width']
            height = fr['height']
            draw.rectangle((left, top, left+width, top+height), outline='red', width=5)    
        font = ImageFont.truetype("public_semana-8_Arial_Unicode.ttf", 10)
        draw.text((50,50), "Predominant beard", font= font, fill="black")
        image.show() 

    if __name__ == "__main__":
        picture = input("Ingrese el path de la imagen:")
        emotions(picture)
    new_list0 = "\n" "Beard most porcentage" "\n"
    REGISTER.append(new_list0)

# o.

def o(data):
    general_list = list()
    for z in data:
        faceAtributes_o = z["faceAttributes"]
        glasses_o = faceAtributes_o["glasses"]
        gender_o = faceAtributes_o["gender"]
        faceId_o = z["faceId"]
        new_list = [faceId_o, gender_o, glasses_o]
        general_list.append(new_list)
    new_list_2 = list()
    for x in general_list:
        if x[1] == "female":
            new_list_2.append(x)
    else:
        pass
    print("\n", new_list_2, "\n")
    register = "Glases of the person(s): " + str(new_list_2)
    REGISTER.append("\n", register, "\n")

# p. 

def p(date):
    for p_1 in data:
        faceAtributes_p = p_1["faceAttributes"]
    gender_p = faceAtributes_p["gender"]
    faceId_p = p_1["faceId"]
    p = p_1["faceAttributes"]
    hair_p = p["hair"]
    hairColor = hair_p["hairColor"]
    colors = hairColor
    if gender_p == "male" and colors == "blond":
            print("\n" "The male person is: ", faceId_p, colors, "\n")
    else:
            print("\n" "There´s nobody with blonde hair" "\n")
    register = "\n" "Blond hair person: ", colors, "\n"
    REGISTER.append(register)
    #print(colors)

# q1. 

def q1(data):
    general_list = []

    for face in data:
        face_id = face['faceId']
        faceAttributes = face['faceAttributes']
        emotion = faceAttributes['emotion']
        less_list = []
        less_list.append(face_id)
        less_list2 = []
        for x, y in emotion.items():
            less_list2.append(x)
            less_list2.append(y)
        less_list.append(less_list2)
        general_list.append(less_list)

    print(general_list)
    register = "All emotion of the persons: ", str("\n", general_list, "\n")
    REGISTER.append(register)

# q2. 

def q2(data):
    high = 0
    for q2 in data:
        dict_faceAttributes = q2["faceAttributes"]
        age = dict_faceAttributes["age"]
        if age > high:
            high = age
            high_dict_faceAttributes = dict_faceAttributes
        hair = high_dict_faceAttributes["hair"]
        bald = hair["bald"]
    print(bald)
    register = "Bald of the persons: " + str("\n",bald,"\n")
    REGISTER.append(register)

# s. Reporte de consutas con al menos 5 imágenes en las que aparezcan
# más de 5 personas por imagen. El reporte debe incluir todas las
# consultas descritas anteriormente


#Clear the register
def s(data):
    REGISTER.clear()
    clear_the_terminal


def options():
    """The function print a menu for user. 
    """
    print("\t" "a. ♦ Show the amount faces and your ages ♦" "\n")
    print("\t" "b. ♦ Swow a list with all the ages and oldest person ♦" "\n")
    print("\t" "c. ♦ List with sublists to show the faceId, age and gender ♦" "\n")
    print("\t" "d. ♦ A list with sublists to show the age and gender, in rank of 10." "\n")
    print("\t" "e. ♦ Show the ages in a list and finally the faceId and age of the person that is closest to the average age with a range of + - 5 years ♦" "\n")
    print("\t" "f. ♦ A list with sublists to show the age and gender of the faces. People are separated by gender in sublists and the masculine ones appear first ♦" "\n")
    print("\t" "g. ♦ Displays faceIds, ages, and genders in a list with sublists. Sorted from least to greatest with the insertion sort method. ♦" "\n")
    print("\t" "h. ♦ Show the faceId and the most predominant hair color of each person. ♦" "\n")
    print("\t" "i. ♦ Shows the faceId and the most predominant emotion. ♦" "\n")
    print("\t" "j. ♦ Show what accessories the person is wearing. ♦" "\n")
    print("\t" "k. ♦ Show the image with a rectangle in the location of the person's face. ♦" "\n")
    print("\t" "l. ♦ Show the person who has the highest percentage of happiness and who is below the average age of the people in the photo. ♦" "\n")
    print("\t" "m. ♦ Show the person who has the highest percentage of happiness and who is above the average age of the people in the photo. ♦" "\n")
    print("\t" "n. ♦ Show a rectangle on the face of people with more beards, sideburns and mustaches ♦" "\n")
    print("\t" "o. ♦ Show female people who wear glasses. ♦" "\n")
    print("\t" "p. ♦ Show the amount faces and older persons ♦" "\n")
    print("\t" "q1. ♦ Show people's emotions by creating a list where sublists come, in turn each sublist contains the face_id and a list with the emotions of each person. ♦" "\n")
    print("\t" "q2. ♦ code to display the bald of the face that is the oldest. ♦" "\n")
    print("\t" "r. ♦ Register ♦" "\n")
    print("\t" "s. Clear the register" "\n")
    print("\t" "0. Exit" "\n")


print("\n" "Enter your birth date" "\n")
clear_the_terminal()

def age_user(birth):
    """Calculate the exact age of the user

    Args:
        birth (tuple) -- Contains the user's year, month, and day of birth in a tuple

    Returns:
        int -- Return the  age of the user
    """    
    actual_date = date.today()
    results = actual_date.year - birth.year 
    results -= ((actual_date.month, actual_date.day) < (birth.month, birth.day))
    return results 
birth_day = date(int(input("Year: ")), int(input("Month: ")), int(input("Day: ")))  
age = age_user(birth_day)
#print(f" Your age is, {age} years old")

if age >= 18:
    print(f"Your age is, {age} years old, you are of legal age, ¡Perfect!" "\n")


else:
    print(f"You are, {age} years old, ¡Sorry! you are a minor, bye." "\n")
    exit()


user_1 = input("Enter your user name: " "\n")

while True:
    password_1 = input("Enter a password: ")
    password_2 = input("Confirm the password: ")
    if password_2 == password_1:

        print("Welcome, ", user_1 )
        break
    else:
        print("Passwords do not match, try again: " "\n")

count = 3
atte = 0

while True:
    if count == -1:
        print("♦ Exceeded maximum attempts, bye. ♦")
        exit()

    confirm_paswword = input("Enter Your Password to continue: ")
    os.system("cls")
    if confirm_paswword == password_1:
        break
    else:
        print("♣ Invalid password ♣" "\n", "you have", count, "attempts left")
    count -= 1



def menu(picture):
    """The function allows the user to interact with the menu. 

    Args:
        picture (str): The function menu(picture) returns what was entered by the user.
    """


        
    
    while True:
        print("\n" "♦ Do you want to make another query? ♦", user_1, "\n")
        
        
        option = input("\t" "Enter the desired value: \n")

        if option == "0":
            print("♣ Thanks, Bye ♣")
            exit()
     
        elif option == "a":
            a(picture)
            clear_the_terminal()
        elif option == "b":
            b(picture)
            clear_the_terminal()
        elif option == "c":
            c(picture)
            clear_the_terminal()
        elif option == "d":
            d(picture)
            clear_the_terminal()
        elif option == "e":
            e(picture)
            clear_the_terminal()
        elif option == "f":
            f(picture)
            clear_the_terminal()
        elif option == "g":
            g(picture)
            clear_the_terminal()
        elif option == "h":
            h(picture)
            clear_the_terminal()
        elif option == "i":
            i(picture)
            clear_the_terminal()
        elif option == "j":
            j(picture)
            clear_the_terminal()      
        elif option == "k":
            k(picture)
            clear_the_terminal()
        elif option == "l":
            l(picture)
            clear_the_terminal()
        elif option == "m":
            m(picture)
            clear_the_terminal()
        elif option == "n":
            n(picture)
            clear_the_terminal()
        elif option == "o":
            o(picture)
            clear_the_terminal()
        elif option == "p":
            p(picture)
            clear_the_terminal()
        elif option == "q1":
            q1(picture)
            clear_the_terminal()
        elif option == "q2":
            q2(picture)
            clear_the_terminal()
        elif option == "r":
            print_register()
            clear_the_terminal()
        elif option == "s":
            s(picture)
            clear_the_terminal()

        else:
            print("☻ Error, please enter a valid value ☻")
            clear_the_terminal()

if __name__ == "__main__":
    picture = input("Enter the path of the image: \n")
    options()
    data = emotions(picture)
    menu(data)






# C:\Users\Usuario\OneDrive\Documentos\Luxgie Murillo\I Semestre\Taller de Programación\Semana 8\me.jpeg
# C:\Users\Usuario\OneDrive\Documentos\Luxgie Murillo\I Semestre\Taller de Programación\Semana 8\barba.jpg
# C:\Users\Usuario\Downloads\me.jpeg
